FROM php:5.6-apache

RUN a2enmod rewrite

# install the PHP extensions we need
RUN apt-get update && apt-get install -y libpng12-dev libjpeg-dev git && rm -rf /var/lib/apt/lists/* \
	&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
	&& docker-php-ext-install gd && docker-php-ext-install mysqli

ENV WORDPRESS_VERSION 4.8
ENV WORDPRESS_UPSTREAM_VERSION 4.8
ENV WORDPRESS_SHA1 3738189a1f37a03fb9cb087160b457d7a641ccb4

RUN mkdir -p /wordpress && curl -o wordpress.tar.gz -SL https://wordpress.org/wordpress-${WORDPRESS_UPSTREAM_VERSION}.tar.gz \
	&& echo "$WORDPRESS_SHA1 *wordpress.tar.gz" | sha1sum -c - \
	&& tar -xzf wordpress.tar.gz -C /wordpress \
	&& mv /wordpress/wordpress/* /wordpress \
	&& rmdir /wordpress/wordpress \
  && rm -rf /wordpress/wp-content \
  && rm -f /wordpress/wp-config.php \
	&& rm wordpress.tar.gz \
  && mkdir -p /wordpress/wp-content/uploads

CMD ["apache2-foreground"]
